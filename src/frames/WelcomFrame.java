package frames;

import core.Main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by motok on 05.01.2017.
 */
public class WelcomFrame extends JFrame{
    private JButton buttonStart, buttonInfo;
    public void createGUI(){
        setResizable(false);
        setSize(520, 540);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLayout(new GridLayout(2,1));
        buttonStart = new JButton("Start");
        buttonInfo = new JButton("About Programm");
        buttonStart.addActionListener(e -> {
            Main.getGameFrame().setVisible(true);
            Main.getGameFrame().startGame();
            Main.getWelcomFrame().setVisible(false);

        });
        add(buttonStart);
        add(buttonInfo);
        setVisible(true);
    }
}
