package frames;

import game.elements.Game;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

/**
 * Created by Vladislav on 11.03.2016.
 */
public class GameFrame extends JFrame {
    Game game;

    public GameFrame() {
        super("Snake");
        game = new Game();
        createGUI();
    }
    public void stopGame(){
        game.stop();
    }
    public void createGUI(){

        setResizable(false);
        setSize(520, 540);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

    }

    public void startGame() {
        Game.label.setVisible(true);
        Game.label.setBounds(200, 0, 600, 10);
        add(Game.label,BorderLayout.NORTH);
        addKeyListener(game);
        remove(game.getCanvas());
        add(game.getCanvas(), BorderLayout.CENTER);
        setVisible(true);
        game.start();
    }
}
