package core;

import frames.GameFrame;
import frames.WelcomFrame;

/**
 * Created by motok on 05.01.2017.
 */
public class Main {
    private static  GameFrame gameFrame;
    private static WelcomFrame welcomFrame;
    public static void main(String[] args) {

        welcomFrame = new WelcomFrame();
        welcomFrame.createGUI();
        welcomFrame.setVisible(true);
        gameFrame = new GameFrame();
        gameFrame.startGame();
        gameFrame.stopGame();
        gameFrame.setVisible(false);
    }

    public static GameFrame getGameFrame() {
        return gameFrame;
    }
    public static WelcomFrame getWelcomFrame(){
        return welcomFrame;
    }


}
