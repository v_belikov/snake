package game.elements;

import java.awt.*;

/**
 * Created by Vladislav on 11.03.2016.
 */
public interface ToCanvas {
    void render(Graphics g);
}
