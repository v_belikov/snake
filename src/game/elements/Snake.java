package game.elements;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by Vladislav on 11.03.2016.
 */
public class Snake implements ToCanvas {


    private ArrayList<Rectangle> snake;//Тело змейки(координаты и размер каждого блока змейки)


    //Конструктор(то что происходит при создании новой змейки)
    public Snake() {
        snake = new ArrayList();
        snake.add(new Rectangle(480 / 2 - 10, 480 / 2 - 10, 10, 10));
    }
    //Методы

    //Движение(умеет двигаться согласна переданному направлению
    public void move(Game.moves moves) {
        add(snake.get(0).x, snake.get(0).y);
        if (moves == moves.LEFT) {
            snake.get(0).x -= 10;
            if (snake.get(0).x < 10)
                snake.get(0).x = 450;

        }
        if (moves == moves.RIGTH) {
            snake.get(0).x += 10;
            if (snake.get(0).x > 450)
                snake.get(0).x = 10;

        }
        if (moves == moves.UP) {
            snake.get(0).y -= 10;
            if (snake.get(0).y < 10)
                snake.get(0).y = 450;
        }
        if (moves == moves.DOWN) {
            snake.get(0).y += 10;
            if (snake.get(0).y > 450)
                snake.get(0).y = 10;
        }
        swap(snake.get(1), snake.get(snake.size() - 1));
        for (int i = 2; i < snake.size() - 1; i++) {
            swap(snake.get(i), snake.get(i + 1));
        }
        snake.remove(snake.size() - 1);
    }

    //Питание (умеет съедать определенный продукт)
    public void eat() {
        int x = snake.get(snake.size() - 1).x;
        int y = snake.get(snake.size() - 1).y;
        add(x, y);
    }

    //Смотреть/проверять (проверяет совпадает ли переданная точка с головой змейки)
    public boolean overlapHead(Point point) {

        return (snake.get(0).x == point.x && snake.get(0).y == point.y);


    }

    //Добовляет к телу змейки один блок
    private void add(int x, int y) {
        snake.add(new Rectangle(x, y, 10, 10));
    }


    //Метод для прорисовки змейки
    @Override
    public void render(Graphics g) {
        Rectangle rect = snake.get(0);
        g.setColor(Color.blue);
        g.fillArc(rect.x, rect.y, rect.width, rect.height, 0, 360);

        g.setColor(Color.green);

        for (int i = 1; i < snake.size(); i++) {
            rect = snake.get(i);
            g.fillArc(rect.x, rect.y, rect.width, rect.height, 0, 360);
        }
    }

    //Смотреть/проверять (проверяет совпадает ли переданная точка со всей змейкой)
    public boolean overlaps(Point point) {
        for (Rectangle rect : snake)
            if (rect.x == point.x && rect.y == point.y)
                return true;
        return false;

    }
    /*
    for(int i =1 ; i <snake.size();i++)
          if(overlapHead(new Point(snake.get(i).x, snake.get(i).y )))
            return true;
      return false;
     */


    private void swap(Rectangle rect1, Rectangle rect2) {
        int x = rect1.x;
        rect1.x = rect2.x;
        rect2.x = x;

        int y = rect1.y;
        rect1.y = rect2.y;
        rect2.y = y;
    }
}
