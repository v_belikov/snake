package game.elements;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by Vladislav on 11.03.2016.
 */
public class MyCanvas extends JPanel {
    protected Image buffer = null;//
    protected Color color = Color.BLACK;
    private ArrayList<ToCanvas> renders = new ArrayList<>();

    public MyCanvas() {
        super();
    }

    public void addRender(ToCanvas render) {
        renders.add(render);
    }

    public void removeRenders() {
        renders.clear();
    }

    public void paintWorld(Graphics g) {
        g.clearRect(0, 0, getWidth(), getHeight());
        g.setColor(color);
        g.fillRect(0, 0, getWidth(), getHeight());
        //Этот цикл
        for (ToCanvas render : renders) {
            render.render(g);
        }
    }

    @Override
    public void paint(Graphics g) {
        //super.paint(g);
        if (buffer == null)
            buffer = createImage(getWidth(), getHeight());
        paintWorld(buffer.getGraphics());
        g.drawImage(buffer, 20, 20, null);
    }
}
