package game.elements;

import java.awt.*;

/**
 * Created by Vladislav on 11.03.2016.
 */
public class Fruit extends Rectangle implements ToCanvas {
    public static int count_fruit = 0;

    public Fruit(Point point) {
        super(point.x, point.y, 10, 10);
        count_fruit++;
    }

    public Point getPoint() {
        return new Point(this.x, this.y);
    }

    public void disopse() {
        count_fruit--;
    }

    @Override
    public void render(Graphics g) {
        g.setColor(Color.RED);
        g.fillRect(x, y, 10, 10);

    }
}
