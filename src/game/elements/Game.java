package game.elements;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

/**
 * Created by Vladislav on 11.03.2016.
 */
public class Game implements Runnable, KeyListener {
    public static int SCORE = 0;
    public static JLabel label = new JLabel("Количество съеденных вкуснях: " + SCORE);
    private Snake snake;//Просим зарезервировать место
    private moves move;
    private MyCanvas canvas;
    private Fruit fruit;
    private Random random;
    private boolean running;

    public Game() {
        snake = new Snake();//Неопсредственно записываем объект в память
        canvas = new MyCanvas();
        canvas.setSize(480, 480);
        random = new Random();
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (running) {
            if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                if (move == moves.RIGTH)
                    return;
                move = moves.LEFT;
            }
            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                if (move == moves.LEFT)
                    return;
                move = moves.RIGTH;
            }
            if (e.getKeyCode() == KeyEvent.VK_UP) {
                if (move == moves.DOWN)
                    return;
                move = moves.UP;
            }
            if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                if (move == moves.UP)
                    return;
                move = moves.DOWN;
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    public JPanel getCanvas() {
        return canvas;
    }

    @Override
    public void run() {
        while (running) {
            snake.move(move);
            if (Fruit.count_fruit == 0) {
                int x = random.nextInt(440) + 20;
                int y = random.nextInt(440) + 20;
                while (snake.overlaps(new Point(x, y)) || x % 10 != 0 || y % 10 != 0) {
                    x = random.nextInt(440) + 20;
                    y = random.nextInt(440) + 20;
                }
                fruit = new Fruit(new Point(x, y));

            }
            if (snake.overlapHead(fruit.getPoint())) {
                snake.eat();
                fruit.disopse();
                SCORE++;
                label.setText("Количество съеденных вкуснях: " + SCORE);
                fruit = null;
            }
            if (fruit != null && Fruit.count_fruit != 0)
                canvas.addRender(fruit);

            canvas.addRender(snake);

            try {
                Thread.sleep(100 - SCORE);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            canvas.paint(canvas.getGraphics());
            canvas.removeRenders();
        }
    }

    public void start() {
        if (running)
            return;
        running = true;
        run();
    }

    public void stop() {
        running = false;
    }

    public enum moves {
        LEFT, UP, DOWN, RIGTH
    }
}
